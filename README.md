# Web App Starter Kit

## Overview

Based on Google's [Web Starter Kit](https://developers.google.com/web/tools/starter-kit/) is an opinionated boilerplate for web development. Tools for building a great experience across many devices and [performance oriented](#web-performance). Helping you to stay productive following the best practices outlined in Google's [Web Fundamentals](https://developers.google.com/web/fundamentals/). A solid starting point for both professionals and newcomers to the industry.

                                                                                                                                            
## Install
To run your development environment you'll need Node.JS and Yarn. Node.JS is a back-end language written in JS, and Yarn is a package manager (Yarn is the same as NPM, we just use Yarn).

Open up your terminal and run this command
```bash
brew install yarn
```
You now have Node.JS & Yarn installed. You're ready to go!

Clone this repo 
```bash
git clone REPO my-project
```

or download the ZIP file.

Navigate to the directory & install the dependencies
```bash
cd my-project
yarn
```

## Let's Go!
These are a list of commands you need to run your development environment. These commands run build-processes which will make development a hell of a lot easier. Make sure you run these in your project root folder.

Run development - This simulates a web server, so you know how your code will run online.
```
yarn start
```

Build the app - this will minify your code, compress your images as well as run performant and best practice tasks. You never need to edit your built code, just re-compile from your development version.
```
yarn build
```

That's it. You only need to make sure you keep these tasks running in your terminal whilst you edit your code. In development, all your edits will live update & reflect automatically. The build process takes care of performance and best-practice issues, so you can just concerntrate on writing good code :)

## Browser Support

At present, we officially aim to support the last two versions of the following browsers:

* Chrome
* Edge
* Firefox
* Safari
* Opera
* Internet Explorer 9+

This is not to say that Web Starter Kit cannot be used in browsers older than those reflected, but merely that our focus will be on ensuring our layouts work great in the above.

## Troubleshooting

If you find yourself running into issues during installation or running the tools, please check our [Troubleshooting](https://github.com/google/web-starter-kit/wiki/Troubleshooting) guide and then open an [issue](https://github.com/google/web-starter-kit/issues). We would be happy to discuss how they can be solved.

## License

Apache 2.0  
Copyright 2015 Google Inc
