// export class templateNPS 

export default class {
  constructor(data) {
    this._data = data;
    this._validated = false;
    this._currentAnswer;
    this._fieldName = data.fieldName;
    this._required = data.required;
  }

  exportTemplate() {
    let template = `<div id="question-container" class="container animated fadeIn">
    <h1 id="question-title">${this._data.title}</h1>
    <p id="question description">${this._data.description}</p>
    <ul class="select-score selection">
    <li>0</li>
    <li>1</li>
    <li>2</li>
    <li>3</li>
    <li>4</li>
    <li>5</li>
    <li>6</li>
    <li>7</li>
    <li>8</li>
    <li>9</li>
    <li>10</li>
    </ul>
    <div class="error-msg">
    Please select a number above to continue
    </div>
    </div>`
    return template
  };

  initUI() {
    let _this = this;

    if(!this._required){
      this._validated = true;
    }
    
    $('.selection li').click(function (e) {
      $(this).siblings().removeClass("btn--active");
      $(this).addClass("btn--active");
      _this._currentAnswer = $(this).index();
      _this._validated = true;
    });
  }
}
