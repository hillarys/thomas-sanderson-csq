// export class templateNPS

export default class {
  constructor(data) {
    this._data = data;
    this._validated = false;
    this._maxChoices = data.maxChoices;
    this._currentChoices = 0;
    this._currentAnswer = [];
    this._fieldName = data.fieldName;
    this._required = data.required;
  }

  exportTemplate() {
    let template = `<div id="question-container" class="container animated fadeIn">
    <h1 id="question-title">${this._data.title}</h1>
    <p id="question description">${this._data.description}</p>
    <ul class="select-answers selection">
    ${this._data.answers.map((answer, i) => `
    <li data-content="0">${answer}</li>
    `.trim()).join('')}
    </ul>
    <div class="other-wrapper">
    <p>Please write any other comments below:</p>
    <textarea maxlength="200" class="other-data" placeholder="Type in any addtional feedback here"></textarea>
    </div>
    <a class="reset-choices">Click here to reset your choices</a>
    <div class="error-msg">
    Please select ${this._data.maxChoices} choices above to continue
    </div>
    </div>`
    return template
  };

  initUI() {
    let _this = this;
    let _$resetChoices = $('.reset-choices');
    let _$selectionLi = $('.selection li');
    let _$otherWrapper = $(".other-wrapper");
    let _$otherData = $(".other-data");

    if(!this._required){
      this._validated = true;
    }

    // On reset button click event
    _$resetChoices.click(function(e){

      _$otherWrapper.removeClass('show');

      _$selectionLi.each(function(){
        $(this).removeClass("btn--active disabled"); 
        _this._currentChoices = 0;
        _this._currentAnswer = []; 
        _$resetChoices.removeClass('active');
      });

    });
    
    // Update other data when inputted
    _$otherData.bind('input propertychange', updateText);
    
    function updateText(e){
      let arrayVal = Number(_this._maxChoices);
      _this._currentAnswer[arrayVal] = "Other: " + _$otherData.val();
    }

    // On list item click events
    _$selectionLi.click(function (e) {

      if (_this._currentChoices >= _this._maxChoices) {
        return;
      }

      _this._currentChoices ++;
      _this._currentAnswer.push($(this).index()) 

      // Update inner content
      $(this).attr('data-content', _this._currentChoices);
      $(this).addClass("btn--active disabled");

      // If an open question, view additional box
      let selected = ($(this).text());

      if(selected == "Other"){
        _$otherWrapper.addClass('show');
      }

      // Show reset choices button
      if (_this._currentChoices >= 0){
        _$resetChoices.addClass('active');
      }

      // Validation
      if (_this._currentChoices >= _this._maxChoices){
        _this._validated = true;
      } else {
        _this._validated = false;
      }      
    });
  }
}
