import {
  throws
} from "assert";

// export class templateNPS

export default class {
  constructor(data) {
    this._data = data;
    this._validated = false;
    this._currentChoices = 0;
    this._currentAnswer = [];
    this._fieldName = data.fieldName;
    this._required = data.required;
  }

  exportTemplate() {
    let template = `<div id="question-container" class="container animated fadeIn">
    <h1 id="question-title">${this._data.title}</h1>
    <p id="question description">${this._data.description}</p>
    <ul class="score-answers selection">
    ${this._data.answers.map((answer, i) => `
    <li>
    <div class="answer"><span>${answer}</span></div>
    <div class="emoji-overlay">
    <div class="emoji-label">Choose a rating</div>
    <ul>
    ${this._data.choices.map((choice, i) => `
    <li class="emoji" data-title="${choice}"><img class="animated fadeInUp" src="images/emojis/${choice}.png" /></li>
    `).join('')}
    </ul>
    </li>
    `.trim()).join('')}
    </ul>
    <div class="error-msg">
    Please fill in all options above to continue
    </div>
    </div>`
    return template
  };

  initUI() {
    let _this = this;
    let _$answer = $('.answer');
    let _$emoji = $('.emoji');

    if (!this._required) {
      this._validated = true;
    }

    _$answer.click(function (e) {
      $(this).parent().find('.emoji-overlay').show();
    });

    _$emoji.mouseenter(function (e) {
      let _$label = $(this).closest('.emoji-overlay').find('.emoji-label');
      _$label.text($(this).data('title'));
    });

    _$emoji.mouseleave(function (e) {
      let _$label = $(this).closest('.emoji-overlay').find('.emoji-label');
      _$label.text("Choose a rating");
    });

    _$emoji.click(function (e) {

      let title = $(this).data("title");
      let _$overlay = $(this).closest('.emoji-overlay');
      let _$wrapper = $(this).parent().parent().siblings();
      let _$li = _$wrapper.parent();

      // Validation iteration
      if (!_$wrapper.parent().hasClass("btn--active")) {
        _this._currentChoices++;
      }

      // Visual methods
      _$overlay.hide();
      _$li.addClass("btn--active");
      _$wrapper.addClass("btn--" + title);

      // Update answers
      _this._currentAnswer[_$li.index()] = (title); 
      
      // Validation
      if (_this._currentChoices >= _this._data.answers.length) {
        _this._validated = true;
      } else {
        _this._validated = false;
      }
    });
  }
}
