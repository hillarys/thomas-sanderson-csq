// export class templateNPS

export default class {
  constructor(data) {
    this._data = data;
    this._maxchar = 200;
    this._currentchar;
    this._validated = false;
    this._currentAnswer = "";
    this._fieldName = data.fieldName;
    this._required = data.required;
  }

  exportTemplate() {
    let template = `<div id="question-container" class="container animated fadeIn">
      <h1 id="question-title">${this._data.title}</h1>
      <p id="question description">${this._data.description}</p>
      <div class="free-text">
      <textarea id="question-text"></textarea>
      </div>
      <p class="characters-remaining">0 Characters Remaining</p>
      <div class="error-msg">
      Please type in a small description in the text area above
      </div>
      </div>`
    return template
  };

  initUI() {
    let _this = this;
    let _$questionText = $("#question-text");
    let _$questionWrapper = $(".free-text");
    this._currentchar = this._maxchar;
    
    if(!this._required){
      this._validated = true;
    }

    updateText();

    _$questionText.attr('maxlength', this._maxchar);
    _$questionText.bind('input propertychange', updateText);

    function updateText(e){

      _this._currentAnswer = _$questionText.val();

      if(_this._currentchar < 0){
        return;
      }

      if(_this._currentchar == 0){
        _$questionWrapper.addClass("text-disabled");
      } else {
        _$questionWrapper.removeClass("text-disabled");
      }

      let val = _$questionText.val().length;
      _this._currentchar = _this._maxchar - val;
      
      if(_this._currentchar < _this._maxchar){
        _this._validated = true;
      } else {
        _this._validated = false;
      }

      $('.characters-remaining').text(_this._currentchar + " characters remaining")
    }
  }
}
