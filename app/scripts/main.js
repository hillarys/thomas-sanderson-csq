// Import Modules
import {
  registerServiceWorker
} from './registerServiceWorker';
import {
  questions
} from './questions/questions';
import templateNPS from './templates/templateNPS';
import templateScore from './templates/templateScore';
import templateMulti from './templates/templateMulti';
import templateTextbox from './templates/templateTextbox';
import templateCheckbox from './templates/templateCheckbox';

// Jquery Dom Elements
let $uiButtons = $(".ui-button");
let $questionContainer = $("#questions");
let $progressInnerbar = $(".progress-innerbar");
let $progressText = $(".progress-text");

let template;
let totalQuestions = questions.length;
let currentQuestion = 1;
let answers = [];

init();

function init() {
  //Set up URL queries

  //Manually enter a question in the parameter string - this can be turned off in production if not needed
  currentQuestion = getUrlParameters("question");
  if (!currentQuestion) {
    currentQuestion = 1;
  }

  loadQuestion(currentQuestion);
  updateProgressBar();

  //Set up ui buttons
  $uiButtons
    .click(function (e) {
      let buttonData = $(this).data('type');
      buttonPress(buttonData);
    })
}

function buttonPress(type) {

  if (type == "next") {
    // Ensure public property had validation flag before changing page
    if (template) {
      if (!template._validated) {
        $('.error-msg').addClass("error-msg--enabled");
        return
      }
    }

    // Adds answers to answer array
    answers[currentQuestion - 1] = {
      answer: template._currentAnswer,
      fieldName: template._fieldName
    };

    // Submits the data to the Cheetah server on click if needed
    submitData(template._currentAnswer, template._fieldName);
    console.log(answers);
    // Changes question
    currentQuestion++;

    // Completes the CSQ if needed
    if (currentQuestion >= (totalQuestions + 1)) {
      completeCSQ();
      return;
    }

    $('#question-prev').removeClass("inactive");

  } else {
    if (currentQuestion > 1) {
      currentQuestion--;
    }
    if (currentQuestion <= 1) {
      $('#question-prev').addClass("inactive");
    }
  }
  loadQuestion(currentQuestion);
  $('html,body').scrollTop(0);
  updateProgressBar();
}

// Write any conditional statements here 
function checkConditionals() {
  switch (currentQuestion) {
    case 2:
      // Change title of question 2 if question one is a detractor
      if (answers[0].answer <= 8) {
        template._data.title = "What could we improve?";
      } else {
        template._data.title = "Why do you say that?";
      }
      break;
  }
}

function submitData(answer, fieldName) {
  // TO DO
}

// Returns a specific parameter from the URL
function getUrlParameters(name) {
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  if (results) {
    return results[1] || 0;
  }
}

// AJAX Question Data
function loadQuestion(questionNumber) {
  let question = questions[questionNumber - 1];
  switch (question.template) {
    case "NPS":
      template = new templateNPS(question);
      break;
    case "Multi":
      template = new templateMulti(question);
      break;
    case "Textbox":
      template = new templateTextbox(question);
      break;
    case "Checkbox":
      template = new templateCheckbox(question);
      break;
    case "Score":
      template = new templateScore(question);
      break;
  }

  // Any conditional will be updated here
  checkConditionals();

  // Loads exported ES6 template strings
  let exportedHTML = template.exportTemplate();
  $questionContainer.html(exportedHTML);

  // Loads specific UI buttons events for template
  template.initUI();
}

function updateProgressBar() {
  let percentage = Math.round((currentQuestion / totalQuestions) * 100);
  $progressInnerbar.width(percentage + "%");
  $progressText.text(percentage + "% completed");
}

function completeCSQ() {
  window.location.href = "completed.html";
}

// Register Service Worker
registerServiceWorker();
