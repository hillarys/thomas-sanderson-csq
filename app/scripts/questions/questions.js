export let questions = [{
    title: "How likely are you to recommend Thomas Sanderson to friends and family?",
    description: "Rate on a scale from 0-10, from not likely (0) to very likely (10)",
    template: "NPS",
    answer: "",
    required: true,
    fieldName: "question1"
  },
  {
    title: "Why do you say that?", // See conditionals main.js
    description: "Type in a small description below",
    template: "Textbox",
    answer: "",
    required: true,
    fieldName: "question2"
  },
  {
    title: "What were the top 3 reasons for choosing Thomas Sanderson?",
    description: "Choose three options below",
    template: "Multi",
    maxChoices: 3,
    answers: ["The products are bespoke / made to measure", "The quality of the product", "The range of products", "The in home service", "Value for money", "Promotion or sale", "The brand reputation", "Other"],
    answer: "",
    required: true,
    fieldName: "question3"
  },
  {
    title: "How would you rate the Design consultation booking process?",
    description: "Rate on a scale from 0-10, from not likely (0) to very likely (10)",
    template: "NPS",
    answer: "",
    required: true,
    fieldName: "question4"
  },
  {
    title: "Please select the areas we could improve on",
    description: "Select up to two options below",
    template: "Checkbox",
    maxChoices: 2,
    answers: ["Website content", "Product information", "Requesting an appointment via the web", "Requesting an appointment via the telephone", "Availability of dates", "Other"],
    answer: "",
    required: true,
    fieldName: "question5"
  },
  {
    title: "How would you rate the Design Consultation appointment?",
    description: "Rate on a scale from 0-10, from not likely (0) to very likely (10)",
    template: "NPS",
    answer: "",
    required: true,
    fieldName: "question6"
  },
  {
    title: "Please select the areas we could improve on",
    description: "Select up to two options below",
    template: "Checkbox",
    maxChoices: 2,
    answers: ["Product knowledge", "Solutions available", "Pricing of the products", "Length of appointment", "Offers available", "Other"],
    answer: "",
    required: true,
    fieldName: "question7"
  },
  {
    title: "How would you rate the installation of the product?",
    description: "Rate on a scale from 0-10, from not likely (0) to very likely (10)",
    template: "NPS",
    answer: "",
    required: true,
    fieldName: "question8"
  },
  {
    title: "Please select the areas we could improve on",
    description: "Select up to two options below",
    template: "Checkbox",
    maxChoices: 2,
    answers: ["Time between the sale and installation", "Installation booking process", "Length of installation", "Quality of installation", "Other"],
    answer: "",
    required: true,
    fieldName: "question9"
  },
  {
    title: "How happy are you with your product and the Thomas Sanderson service?",
    description: "Rate on a scale from 0-10, from not likely (0) to very likely (10)",
    template: "NPS",
    answer: "",
    required: true,
    fieldName: "question10"
  },
  {
    title: "Please select the areas we could improve on",
    description: "Select up to two options below",
    template: "Checkbox",
    maxChoices: 2,
    answers: ["Quality of product", "Effectiveness of product", "Value for money", "Sales process", "After care service", "Other"],
    answer: "",
    required: true,
    fieldName: "question11"
  },
  {
    title: "Thinking of your contact with Thomas Sanderson, how would you rate our communication with you?",
    description: "Select all of the options below and rate them",
    template: "Score",
    answers: ["When you booked your Design Consultation", "Between booking an appointment and your appointment", "Between having the appointment and placing the order", "After you placed the order", "After the product was fitted"],
    choices: ["Bad", "Okay", "Good"],
    answer: "",
    required: true,
    fieldName: "question13"
  },
  {
    title: "How could we improve our communications?",
    description: "Type in a small description below",
    template: "Textbox",
    answer: "",
    required: true,
    fieldName: "question14"
  },
  {
    title: "Describe in one word how the new product made you feel?",
    description: "Type in a small description below",
    template: "Textbox",
    answer: "",
    required: true,
    fieldName: "question15"
  }, 
  {
    title: "Finally, is there one thing that Thomas Sanderson could have done to enhance your experience in dealing with us?",
    description: "Type in a small description below",
    template: "Textbox",
    answer: "",
    required: true,
    fieldName: "question16"
  }
]
